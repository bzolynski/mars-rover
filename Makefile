all : composer
.PHONY: all start stop composer build phpunit

build :
	cd docker && ./build.sh && cd .. && make composer

start :
	docker-compose up -d

stop :
	docker-compose down

composer :
	docker-compose run --rm --no-deps php composer install

composer-require :
	docker-compose run --rm --no-deps php composer require $(package)

composer-require-dev :
	docker-compose run --rm --no-deps php composer require --dev $(package)

composer-remove-dev :
	docker-compose run --rm --no-deps php composer remove --dev $(package)

phpunit :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests

phpunit-coverage-text :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-text

phpunit-coverage-html :
	docker-compose run --rm --no-deps php vendor/bin/phpunit tests --coverage-html coverage/
