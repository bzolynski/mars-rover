<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application;

use CODEfactors\MarsRover\Application\Sequence\PlateauSequence;
use CODEfactors\MarsRover\Application\Sequence\RoverCoordinatesSequence;
use CODEfactors\MarsRover\Application\Sequence\RoverMovementSequence;

class DataSequencer
{
    const PLATEAU_SEQUENCE = 1;

    const ROVER_COORDINATES_SEQUENCE = 2;

    const ROVER_MOVEMENT_SEQUENCE = 3;

    private $inputDataSequence;

    private $plateau;

    private $rover;

    private $response = '';

    public function __construct()
    {
        $this->inputDataSequence = self::PLATEAU_SEQUENCE;
    }

    public function receive(string $inputData)
    {
        $data = explode(' ', $inputData);
        if (count($data) === 2) {
            $this->setPlateau($inputData);
        } else if (count($data) === 3) {
            $this->setRoverCoordinates($inputData);
        } else if (count($data) === 1) {
            $this->response = $this->moveRover($inputData);
        } else {
            throw new InvalidInputDataException('Unknown data provided');
        }
        $this->advanceSequence();
    }

    public function hasResponse(): bool
    {
        return $this->response !== '';
    }

    public function getResponse(): string
    {
        $response = $this->response;
        $this->response = '';
        return $response;
    }

    private function isPlateauSequence(): bool
    {
        return $this->inputDataSequence === self::PLATEAU_SEQUENCE;
    }

    private function isRoverCoordinatesSequence(): bool
    {
        return $this->inputDataSequence === self::ROVER_COORDINATES_SEQUENCE;
    }

    private function isRoverMovementSequence(): bool
    {
        return $this->inputDataSequence === self::ROVER_MOVEMENT_SEQUENCE;
    }

    private function advanceSequence(): void
    {
        if ($this->isPlateauSequence()) {
            $this->inputDataSequence = self::ROVER_COORDINATES_SEQUENCE;
        } else if ($this->isRoverCoordinatesSequence()) {
            $this->inputDataSequence = self::ROVER_MOVEMENT_SEQUENCE;
        } else if ($this->isRoverMovementSequence()) {
            $this->inputDataSequence = self::ROVER_COORDINATES_SEQUENCE;
        }
    }

    public function setPlateau(string $inputData): void
    {
        if (!$this->isPlateauSequence()) {
            throw new InvalidInputDataException('Invalid sequence');
        }
        $plateauSequence = new PlateauSequence();
        $this->plateau = $plateauSequence->execute($inputData);
    }

    public function setRoverCoordinates(string $inputData): void
    {
        if (!$this->isRoverCoordinatesSequence()) {
            throw new InvalidInputDataException('Invalid sequence');
        }
        $coordinatesSequence = new RoverCoordinatesSequence($inputData, $this->plateau);
        $this->rover = $coordinatesSequence->execute();
    }

    public function moveRover(string $inputData): string
    {
        if (!$this->isRoverMovementSequence()) {
            throw new InvalidInputDataException('Invalid sequence');
        }
        $movementSequence = new RoverMovementSequence($this->rover);
        return $movementSequence->execute($inputData);
    }
}
