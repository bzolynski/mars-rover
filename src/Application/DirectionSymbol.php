<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application;

use CODEfactors\MarsRover\Domain\Direction\DirectionInterface;
use CODEfactors\MarsRover\Domain\Direction\EastDirection;
use CODEfactors\MarsRover\Domain\Direction\NorthDirection;
use CODEfactors\MarsRover\Domain\Direction\SouthDirection;
use CODEfactors\MarsRover\Domain\Direction\WestDirection;

class DirectionSymbol
{
    public static function getSymbolByObject(DirectionInterface $direction): string
    {
        if ($direction instanceof NorthDirection) {
            return 'N';
        } else if ($direction instanceof EastDirection) {
            return 'E';
        } else if ($direction instanceof SouthDirection) {
            return 'S';
        } else {
            return 'W';
        }
    }

    public static function getObjectBySymbol(string $symbol): DirectionInterface
    {
        switch ($symbol) {
            case 'N':
                return new NorthDirection();
            case 'E':
                return new EastDirection();
            case 'S':
                return new SouthDirection();
            case 'W':
                return new WestDirection();
            default:
                throw new InvalidInputDataException('Symbol not recognized');
            }
    }
}
