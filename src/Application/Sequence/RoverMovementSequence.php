<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application\Sequence;

use CODEfactors\MarsRover\Application\DirectionSymbol;
use CODEfactors\MarsRover\Domain\Rover\Rover;
use CODEfactors\MarsRover\Application\InvalidInputDataException;

class RoverMovementSequence
{
    private $rover;

    public function __construct(Rover $rover)
    {
        $this->rover = $rover;
    }

    public function execute(string $movementData): string
    {
        for ($i = 0; $i < strlen($movementData); $i += 1) {
            $command = substr($movementData, $i, 1);
            if ($command === 'L') {
                $this->rover->rotateLeft();
            } else if ($command === 'R') {
                $this->rover->rotateRight();
            } else if ($command === 'M') {
                $this->rover->move();
            } else {
                throw new InvalidInputDataException('Unrecognized move');
            }
        }

        return $this->rover->getCoordinates()->getX() . ' ' .
            $this->rover->getCoordinates()->getY() . ' ' .
            DirectionSymbol::getSymbolByObject($this->rover->getDirection());
    }
}
