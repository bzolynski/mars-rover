<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application\Sequence;

use CODEfactors\MarsRover\Application\DirectionSymbol;
use CODEfactors\MarsRover\Domain\Coordinates;
use CODEfactors\MarsRover\Domain\Plateau\Plateau;
use CODEfactors\MarsRover\Domain\Rover\Rover;

class RoverCoordinatesSequence
{
    private $x;

    private $y;

    private $directionSymbol;

    private $plateau;

    public function __construct(string $coordinatesData, Plateau $plateau)
    {
        $data = explode(' ', $coordinatesData);
        $this->x = intval($data[0]);
        $this->y = intval($data[1]);
        $this->directionSymbol = $data[2];
        $this->plateau = $plateau;
    }

    public function execute(): Rover
    {
        $coordinates = new Coordinates($this->x, $this->y);
        $direction = DirectionSymbol::getObjectBySymbol($this->directionSymbol);
        return new Rover($coordinates, $direction, $this->plateau);
    }
}
