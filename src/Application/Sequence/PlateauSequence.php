<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application\Sequence;

use CODEfactors\MarsRover\Domain\Plateau\Plateau;

class PlateauSequence
{
    public function execute(string $inputData)
    {
        $data = explode(' ', $inputData);
        return new Plateau(intval($data[0]), intval($data[1]));
    }
}
