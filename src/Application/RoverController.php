<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Application;

class RoverController
{
    private $sequencer;

    private $response = [];

    private $responseIndex = -1;

    public function __construct()
    {
        $this->sequencer = new DataSequencer();
    }

    public function receiveInputData(string $inputData)
    {
        $this->sequencer->receive($inputData);
        if ($this->sequencer->hasResponse()) {
            $this->response[] = $this->sequencer->getResponse();
        }
    }

    public function receiveOutputData(): string
    {
        $this->responseIndex += 1;
        return $this->response[$this->responseIndex];
    }
}
