<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Plateau;

use CODEfactors\MarsRover\Domain\Coordinates;

class Plateau
{
    private $xMax;

    private $yMax;

    public function __construct(int $xMax, int $yMax)
    {
        $this->validateSize($xMax, $yMax);
        $this->xMax = $xMax;
        $this->yMax = $yMax;
    }

    private function validateSize(int $xMax, int $yMax)
    {
        if ($xMax < 1) {
            throw new InvalidPlateauSizeException('Invalid size x');
        }
        if ($yMax < 1) {
            throw new InvalidPlateauSizeException('Invalid size y');
        }
    }

    public function contains(Coordinates $coordinates): bool
    {
        return $coordinates->getX() >= 0
            && $coordinates->getX() <= $this->xMax
            && $coordinates->getY() >= 0
            && $coordinates->getY() <= $this->yMax;
    }
}
