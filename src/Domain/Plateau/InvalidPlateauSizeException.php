<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Plateau;

use Exception;

class InvalidPlateauSizeException extends Exception
{

}
