<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Rover;

use CODEfactors\MarsRover\Domain\Coordinates;
use CODEfactors\MarsRover\Domain\Direction\DirectionInterface;
use CODEfactors\MarsRover\Domain\Plateau\Plateau;

class Rover
{
    private $coordinates;

    private $direction;

    private $plateau;

    public function __construct(Coordinates $coordinates, DirectionInterface $direction, Plateau $plateau)
    {
        $this->direction = $direction;
        $this->plateau = $plateau;
        $this->validateCoordinatesAgainstPlateau($coordinates);
        $this->coordinates = $coordinates;
    }

    public function rotateLeft()
    {
        $this->direction = $this->direction->rotateLeft();
    }

    public function rotateRight()
    {
        $this->direction = $this->direction->rotateRight();
    }

    public function move()
    {
        $offset = $this->direction->move();
        $newCoordinates = $this->coordinates->withOffset($offset);
        $this->validateCoordinatesAgainstPlateau($newCoordinates);
        $this->coordinates = $newCoordinates;
        $this->makePhotoOfTerrain();
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }

    public function getDirection(): DirectionInterface
    {
        return $this->direction;
    }

    private function validateCoordinatesAgainstPlateau(Coordinates $coordinates)
    {
        if (!$this->plateau->contains($coordinates)) {
            throw new RoverOutOfBorderException();
        }
    }

    private function makePhotoOfTerrain()
    {
        // TODO: To be implemented by NASA
    }
}
