<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Rover;

use Exception;

class RoverOutOfBorderException extends Exception
{

}
