<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Direction;

use CODEfactors\MarsRover\Domain\Offset;

class SouthDirection implements DirectionInterface
{
    public function rotateLeft(): EastDirection
    {
        return new EastDirection();
    }

    public function rotateRight(): WestDirection
    {
        return new WestDirection();
    }

    public function move(): Offset
    {
        return new Offset(0, -1);
    }
}
