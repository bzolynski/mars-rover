<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Direction;

use CODEfactors\MarsRover\Domain\Offset;

class NorthDirection implements DirectionInterface
{
    public function rotateLeft(): WestDirection
    {
        return new WestDirection();
    }

    public function rotateRight(): EastDirection
    {
        return new EastDirection();
    }

    public function move(): Offset
    {
        return new Offset(0, 1);
    }
}
