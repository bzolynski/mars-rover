<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Direction;

use CODEfactors\MarsRover\Domain\Offset;

class EastDirection implements DirectionInterface
{
    public function rotateLeft(): NorthDirection
    {
        return new NorthDirection();
    }

    public function rotateRight(): SouthDirection
    {
        return new SouthDirection();
    }

    public function move(): Offset
    {
        return new Offset(1, 0);
    }
}
