<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Direction;

use CODEfactors\MarsRover\Domain\Offset;

class WestDirection implements DirectionInterface
{
    public function rotateLeft(): SouthDirection
    {
        return new SouthDirection();
    }

    public function rotateRight(): NorthDirection
    {
        return new NorthDirection();
    }

    public function move(): Offset
    {
        return new Offset(-1, 0);
    }
}
