<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain\Direction;

use CODEfactors\MarsRover\Domain\Offset;

interface DirectionInterface
{
    public function rotateLeft();

    public function rotateRight();

    public function move(): Offset;
}
