<?php

declare(strict_types=1);

namespace CODEfactors\MarsRover\Domain;

class Offset
{
    private $x;

    private $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }
}
