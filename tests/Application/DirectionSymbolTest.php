<?php

declare(strict_types=1);

namespace CODEfactors\Tests\MarsRover\Application;

use CODEfactors\MarsRover\Application\DirectionSymbol;
use CODEfactors\MarsRover\Application\InvalidInputDataException;
use CODEfactors\MarsRover\Domain\Direction\EastDirection;
use CODEfactors\MarsRover\Domain\Direction\NorthDirection;
use CODEfactors\MarsRover\Domain\Direction\SouthDirection;
use CODEfactors\MarsRover\Domain\Direction\WestDirection;
use PHPUnit\Framework\TestCase;

class DirectionSymbolTest extends TestCase
{
    public function testGettingSymbolByObject()
    {
        $this->assertSame('N', DirectionSymbol::getSymbolByObject(new NorthDirection()));
        $this->assertSame('W', DirectionSymbol::getSymbolByObject(new WestDirection()));
        $this->assertSame('S', DirectionSymbol::getSymbolByObject(new SouthDirection()));
        $this->assertSame('E', DirectionSymbol::getSymbolByObject(new EastDirection()));
    }

    public function testGettingObjectBySymbol()
    {
        $this->assertInstanceOf(NorthDirection::class, DirectionSymbol::getObjectBySymbol('N'));
        $this->assertInstanceOf(WestDirection::class, DirectionSymbol::getObjectBySymbol('W'));
        $this->assertInstanceOf(SouthDirection::class, DirectionSymbol::getObjectBySymbol('S'));
        $this->assertInstanceOf(EastDirection::class, DirectionSymbol::getObjectBySymbol('E'));
    }

    public function testGettingObjectForInvalidSymbol()
    {
        $this->expectException(InvalidInputDataException::class);
        DirectionSymbol::getObjectBySymbol('X');
    }
}
