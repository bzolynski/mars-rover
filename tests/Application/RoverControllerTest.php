<?php

declare(strict_types=1);

namespace CODEfactors\Tests\MarsRover\Application;

use CODEfactors\MarsRover\Application\InvalidInputDataException;
use CODEfactors\MarsRover\Application\RoverController;
use PHPUnit\Framework\TestCase;

class RoverControllerTest extends TestCase
{
    public function testMissionOfTwoRovers()
    {
        $ctrl = new RoverController();
        $ctrl->receiveInputData('5 5');
        $ctrl->receiveInputData('1 2 N');
        $ctrl->receiveInputData('LMLMLMLMM');
        $ctrl->receiveInputData('3 3 E');
        $ctrl->receiveInputData('MMRMMRMRRM');
        $this->assertSame('1 3 N', $ctrl->receiveOutputData());
        $this->assertSame('5 1 E', $ctrl->receiveOutputData());
    }

    public function testUnrecognizedSequence()
    {
        $this->expectException(InvalidInputDataException::class);
        $ctrl = new RoverController();
        $ctrl->receiveInputData('1 1 1 1');
    }

    public function testInvalidSequenceOrderOnPlateauStep()
    {
        $this->expectException(InvalidInputDataException::class);
        $ctrl = new RoverController();
        $ctrl->receiveInputData('5 5');
        $ctrl->receiveInputData('1 2 N');
        $ctrl->receiveInputData('5 5');
    }

    public function testInvalidSequenceOrderOnRoverCoordinatesStep()
    {
        $this->expectException(InvalidInputDataException::class);
        $ctrl = new RoverController();
        $ctrl->receiveInputData('5 5');
        $ctrl->receiveInputData('LMLMLMLMMM');
    }

    public function testInvalidSequenceOrderOnRoverMovementStep()
    {
        $this->expectException(InvalidInputDataException::class);
        $ctrl = new RoverController();
        $ctrl->receiveInputData('5 5');
        $ctrl->receiveInputData('1 2 N');
        $ctrl->receiveInputData('1 2 N');
    }

    public function testUnrecognizedMoveForRover()
    {
        $this->expectException(InvalidInputDataException::class);
        $ctrl = new RoverController();
        $ctrl->receiveInputData('5 5');
        $ctrl->receiveInputData('1 2 N');
        $ctrl->receiveInputData('LMLMLMXLM');
    }
}
