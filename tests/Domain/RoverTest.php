<?php

declare(strict_types=1);

namespace CODEfactors\Tests\MarsRover\Domain;

use CODEfactors\MarsRover\Domain\Coordinates;
use CODEfactors\MarsRover\Domain\Direction\EastDirection;
use CODEfactors\MarsRover\Domain\Direction\NorthDirection;
use CODEfactors\MarsRover\Domain\Direction\SouthDirection;
use CODEfactors\MarsRover\Domain\Direction\WestDirection;
use CODEfactors\MarsRover\Domain\Plateau\Plateau;
use CODEfactors\MarsRover\Domain\Rover\Rover;
use CODEfactors\MarsRover\Domain\Rover\RoverOutOfBorderException;
use PHPUnit\Framework\TestCase;

class RoverTest extends TestCase
{
    private $plateau;

    public function setUp()
    {
        $this->plateau = new Plateau(10, 10);
    }

    public function testRoverMoves()
    {
        $coordinates = new Coordinates(5, 5);
        $direction = new WestDirection();
        $rover = new Rover($coordinates, $direction, $this->plateau);

        $rover->move();
        $this->assertSame(4, $rover->getCoordinates()->getX());
        $this->assertSame(5, $rover->getCoordinates()->getY());
        $this->assertInstanceOf(WestDirection::class, $rover->getDirection());

        $rover->rotateLeft();
        $rover->move();
        $this->assertSame(4, $rover->getCoordinates()->getX());
        $this->assertSame(4, $rover->getCoordinates()->getY());
        $this->assertInstanceOf(SouthDirection::class, $rover->getDirection());

        $rover->rotateLeft();
        $rover->move();
        $this->assertSame(5, $rover->getCoordinates()->getX());
        $this->assertSame(4, $rover->getCoordinates()->getY());
        $this->assertInstanceOf(EastDirection::class, $rover->getDirection());

        $rover->rotateLeft();
        $rover->move();
        $this->assertSame(5, $rover->getCoordinates()->getX());
        $this->assertSame(5, $rover->getCoordinates()->getY());
        $this->assertInstanceOf(NorthDirection::class, $rover->getDirection());

        $rover->rotateRight();
        $rover->move();
        $this->assertSame(6, $rover->getCoordinates()->getX());
        $this->assertSame(5, $rover->getCoordinates()->getY());
        $this->assertInstanceOf(EastDirection::class, $rover->getDirection());
    }

    public function testRoverOutOfPlateau()
    {
        $this->expectException(RoverOutOfBorderException::class);
        $coordinates = new Coordinates(10, 11);
        $direction = new NorthDirection();
        new Rover($coordinates, $direction, $this->plateau);
    }

    public function testRoverOutOfNorthBorder()
    {
        $this->expectException(RoverOutOfBorderException::class);

        $coordinates = new Coordinates(5, 8);
        $direction = new NorthDirection();
        $rover = new Rover($coordinates, $direction, $this->plateau);

        $rover->move();
        $rover->move();
        $rover->move();
    }

    public function testRoverOutOfEastBorder()
    {
        $this->expectException(RoverOutOfBorderException::class);

        $coordinates = new Coordinates(8, 5);
        $direction = new EastDirection();
        $rover = new Rover($coordinates, $direction, $this->plateau);

        $rover->move();
        $rover->move();
        $rover->move();
    }

    public function testRoverOutOfSouthBorder()
    {
        $this->expectException(RoverOutOfBorderException::class);

        $coordinates = new Coordinates(5, 2);
        $direction = new SouthDirection();
        $rover = new Rover($coordinates, $direction, $this->plateau);

        $rover->move();
        $rover->move();
        $rover->move();
    }


    public function testRoverOutOfWestBorder()
    {
        $this->expectException(RoverOutOfBorderException::class);

        $coordinates = new Coordinates(2, 5);
        $direction = new WestDirection();
        $rover = new Rover($coordinates, $direction, $this->plateau);

        $rover->move();
        $rover->move();
        $rover->move();
    }
}
