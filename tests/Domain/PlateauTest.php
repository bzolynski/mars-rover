<?php

declare(strict_types=1);

namespace CODEfactors\Tests\MarsRover\Domain;

use PHPUnit\Framework\TestCase;
use CODEfactors\MarsRover\Domain\Plateau\Plateau;
use CODEfactors\MarsRover\Domain\Plateau\InvalidPlateauSizeException;

class PlateauTest extends TestCase
{
    public function testInstantiation()
    {
        $plateau = new Plateau(10, 10);
        $this->assertInstanceOf(Plateau::class, $plateau);
    }

    /**
     * @dataProvider invalidSizesData
     * @var int x
     * @var int y
     */
    public function testInvalidSize(int $x, int $y)
    {
        $this->expectException(InvalidPlateauSizeException::class);
        new Plateau($x, $y);
    }

    public function invalidSizesData(): array
    {
        return [
            [
                0, 10
            ],
            [
                5, 0
            ],
            [
                0, 0
            ],
            [
                -1, 5
            ],
            [
                5, -1
            ],
            [
                -5, -10
            ]
        ];
    }
}
