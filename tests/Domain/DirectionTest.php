<?php

declare(strict_types=1);

namespace CODEfactors\Tests\MarsRover\Domain;

use CODEfactors\MarsRover\Domain\Direction\EastDirection;
use CODEfactors\MarsRover\Domain\Direction\NorthDirection;
use CODEfactors\MarsRover\Domain\Direction\SouthDirection;
use CODEfactors\MarsRover\Domain\Direction\WestDirection;
use PHPUnit\Framework\TestCase;

class DirectionTest extends TestCase
{
    public function testRotateLeftFromNorth()
    {
        $direction = new NorthDirection();
        $this->assertInstanceOf(WestDirection::class, $direction->rotateLeft());
    }

    public function testRotateRightFromNorth()
    {
        $direction = new NorthDirection();
        $this->assertInstanceOf(EastDirection::class, $direction->rotateRight());
    }

    public function testRotateLeftFromEast()
    {
        $direction = new EastDirection();
        $this->assertInstanceOf(NorthDirection::class, $direction->rotateLeft());
    }

    public function testRotateRightFromEast()
    {
        $direction = new EastDirection();
        $this->assertInstanceOf(SouthDirection::class, $direction->rotateRight());
    }

    public function testRotateLeftFromSouth()
    {
        $direction = new SouthDirection();
        $this->assertInstanceOf(EastDirection::class, $direction->rotateLeft());
    }

    public function testRotateRightFromSouth()
    {
        $direction = new SouthDirection();
        $this->assertInstanceOf(WestDirection::class, $direction->rotateRight());
    }

    public function testRotateLeftFromWest()
    {
        $direction = new WestDirection();
        $this->assertInstanceOf(SouthDirection::class, $direction->rotateLeft());
    }

    public function testRotateRightFromWest()
    {
        $direction = new WestDirection();
        $this->assertInstanceOf(NorthDirection::class, $direction->rotateRight());
    }

    public function testNorthOffset()
    {
        $direction = new NorthDirection();
        $this->assertSame(0, $direction->move()->getX());
        $this->assertSame(1, $direction->move()->getY());
    }

    public function testEastOffset()
    {
        $direction = new EastDirection();
        $this->assertSame(1, $direction->move()->getX());
        $this->assertSame(0, $direction->move()->getY());
    }

    public function testSouthOffset()
    {
        $direction = new SouthDirection();
        $this->assertSame(0, $direction->move()->getX());
        $this->assertSame(-1, $direction->move()->getY());
    }

    public function testWestOffset()
    {
        $direction = new WestDirection();
        $this->assertSame(-1, $direction->move()->getX());
        $this->assertSame(0, $direction->move()->getY());
    }
}
