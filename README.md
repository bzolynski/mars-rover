
Mars Rover example
==================

A squad of robotic NASA rovers is going to land on Mars.


Overview
--------

Yet another TDD practice example with Mars Rover Kata.

Domain Layer contains all model artifacts (Rover, Plateau, Coordinates, Directions, etc.).

Application Layer contains Rover Controller as the public API, and Sequencer which receives and analyzes strings of
data.


Technical details
-----------------

- PHP 7.1.3 with Docker Compose
- PHPUnit 6.2
- Composer
- Make commands (just to make it more handy for dev)
- 100% code coverage
- Mars Rover project to be launched by NASA in 2022 ;)


Usage
-----

- Use `make build` to build the project
- Use `make phpunit` to test the project
- Use `make phpunit-coverage-html` to test the project with generated HTML for code coverage

Enjoy!
